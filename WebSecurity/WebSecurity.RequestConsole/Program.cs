﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace WebSecurity.RequestConsole
{
    class Program
    {
        static void Main(string[] args)
        {

            var s = Request("http://localhost:4294/");
        }

        public static string Request(string url)
        {
            HttpClient httpClient =
                new HttpClient(new HttpClientHandler
                {
                    AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate
                });
            //httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpRequestMessage request = new HttpRequestMessage()
            {
                Method = HttpMethod.Post,
                RequestUri = new Uri("http://localhost:4294/"),
            };

            request.Headers.Add("User-Agent",
                "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0");
            request.Headers.Add("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
            request.Headers.Add("Accept-Language", "en-US,en;q=0.5");
            request.Headers.Add("Accept-Encoding", "gzip, deflate");
            request.Content = new StringContent("",
                Encoding.UTF8,
                "application/json"); //CONTENT-TYPE header

            httpClient.DefaultRequestHeaders.ExpectContinue = false;
            HttpResponseMessage response = httpClient.SendAsync(request).Result;
            string html = response.Content.ReadAsStringAsync().Result;
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return html;
            }
            else
            {
                //Write log why it's not ok
                return null;
            }
        }
    }
}
