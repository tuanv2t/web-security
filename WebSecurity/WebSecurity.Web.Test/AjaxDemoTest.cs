﻿using System;
using System.IO;
using System.Net;
using System.Windows.Forms;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RestSharp;

namespace WebSecurity.Web.Test
{
    [TestClass]
    public class AjaxDemoTest
    {
        
        [TestMethod]
        public void HelloTest()
        {
            var client = new RestClient("http://mywebsecuritytest.com/AjaxDemo/Hello");
            var request = new RestRequest(Method.POST);
            request.RequestFormat = DataFormat.Json;
            IRestResponse response = client.Execute(request);//With AjaxOnly Attribute, the request will not have ability to get data from ajax Hello
            Assert.AreEqual(response.StatusCode, HttpStatusCode.NotFound);
        }
        [TestMethod]
        public void HelloAjaxWithHeaderTest()
        {
            var client = new RestClient("http://mywebsecuritytest.com/AjaxDemo/Hello");
            var request = new RestRequest(Method.POST);
            request.AddHeader("X-Requested-With", "XMLHttpRequest");//Ajax request //http://stackoverflow.com/questions/29282190/where-is-request-isajaxrequest-in-asp-net-core-mvc
            request.RequestFormat = DataFormat.Json;
            IRestResponse response = client.Execute(request);
            Assert.AreEqual(response.StatusCode, HttpStatusCode.NotFound);////AjaxOnly Attribute does not prevent this request , the request with request.AddHeader("X-Requested-With", "XMLHttpRequest") will be able to crawl data :(
        }
        //Try to get the whole page 

        [TestMethod]
        public void AjaxDemoPageTest()
        {
            var client = new RestClient("http://mywebsecuritytest.com/AjaxDemo");
            var request = new RestRequest(Method.POST);
            request.RequestFormat = DataFormat.Json;
            IRestResponse response = client.Execute(request);
            Assert.IsFalse(response.Content.Contains("Hello from AjaxDemo"));//check if the "Hello from AjaxDemo" has been bound to the page or not
        }
        [TestMethod]
        public void AjaxDemoPageGetWithHttpWebRequestTest()
        {
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create("http://mywebsecuritytest.com/AjaxDemo");
            request.UserAgent = "A .NET Web Crawler";
            WebResponse response = request.GetResponse();
            Stream stream = response.GetResponseStream();
            StreamReader reader = new StreamReader(stream);
            string htmlText = reader.ReadToEnd();
            Assert.IsFalse(htmlText.Contains("Hello from AjaxDemo"));
        }
        //Try to load page with a Web browser control, which migh be used to crawl
        WebBrowser webBrowser1 = new WebBrowser();
        [TestMethod]
        public void AjaxDemoPageWithWebBrowserControlTest()
        {
            webBrowser1.DocumentCompleted += WebBrowser1_DocumentCompleted;
            var url = "http://mywebsecuritytest.com/AjaxDemo";
            webBrowser1.Navigate(url);
            var content = webBrowser1.DocumentText;
            //Assert.IsFalse(content.Contains("Hello from AjaxDemo"));//Need to use window form WebSecurity.Test.Winform to test

        }

        private void WebBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            //Try to check if the crawled page content contains "Hello from AjaxDemo" or not
            var content = webBrowser1.DocumentText;
        }
        [TestMethod]
        public void AjaxDemoPageGetWithWebClientTest()
        {
            // WebClient object
            WebClient client = new WebClient();

            // Retrieve resource as a stream
            Stream data = client.OpenRead(new Uri("http://mywebsecuritytest.com/AjaxDemo"));

            // Retrieve the text
            StreamReader reader = new StreamReader(data);
            string htmlContent = reader.ReadToEnd();
            Assert.IsFalse(htmlContent.Contains("Hello from AjaxDemo"));//WebClient is also unable to get the html rendered by ajax 

        }
    }
}
