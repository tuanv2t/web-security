﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RestSharp;

namespace WebSecurity.Web.Test
{
    [TestClass]
    public class HomeTest
    {
        [TestMethod]
        public void HomePageWithAjaxTest()
        {
            var client = new RestClient("http://mywebsecuritytest.com/Home/Index");
            var request = new RestRequest(Method.POST);
            request.AddHeader("postman-token", "1b78e07b-7626-9722-5d01-c2d44a49e35c");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddParameter("application/x-www-form-urlencoded", "VersionID=0", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            // Exam the response.Content, see that  <span id="ajaxcontent"></span> has no content  "Hello from AjaxDemo"
            // That means the ajax still not works.
            Assert.IsTrue(response.Content.Contains("<span id=\"ajaxcontent\"></span>"));
            Assert.IsFalse(response.Content.Contains("<span id=\"ajaxcontent\">Hello from AjaxDemo</span>"));
        }
        [TestMethod]
        public void AjaxDemoTest()
        {
            var client = new RestClient("http://mywebsecuritytest.com/Home/AjaxDemo");
            var request = new RestRequest(Method.POST);
            request.RequestFormat = DataFormat.Json;
            IRestResponse response = client.Execute(request);//With [AjaxOnly] attribute, this request will not get data from ajax Hello
        }
        [TestMethod]
        public void YoutradeHomeTest()
        {
            var client = new RestClient("http://youtrade.vn/");
            var request = new RestRequest(Method.POST);
            IRestResponse response = client.Execute(request);
            //Unable to crawl the content which are bound by ajax request
        }
        
    }
}
