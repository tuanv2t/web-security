﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WebSecurity.Web.Startup))]
namespace WebSecurity.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
