﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebSecurity.Web.Securities;

namespace WebSecurity.Web.Controllers
{
    public class AjaxDemoController : Controller
    {
        public ActionResult Index()
        {
            var header = HttpContext.Request.Headers;
            return View();
        }

        
        [HttpPost]
        [AjaxOnly]//AjaxOnly works well to prevent crawling ( see demo in  WebSecurity.Web.Test > AjaxDemoTest > HelloTest
        public ActionResult Hello()
        {
            //Test with AjaxOnly
            //Check if another tools such as crawler tools are able to call this ajax or not
            return Json(new {data = "Hello from AjaxDemo" });
        }
    }
}