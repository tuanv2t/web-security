﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebSecurity.Web.Securities;

namespace WebSecurity.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var header = HttpContext.Request.Headers;
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        [HttpPost]
        //[AjaxOnly]
        public ActionResult AjaxDemo()
        {
            //Test with AjaxOnly
            //Check if another tools such as crawler tools are able to call this ajax or not
            return Json(new {data = "Hello from AjaxDemo" });
        }
    }
}